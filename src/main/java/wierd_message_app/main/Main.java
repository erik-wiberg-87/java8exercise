package wierd_message_app.main;

import static wierd_message_app.domain.MessageCategory.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import wierd_message_app.domain.Message;
import wierd_message_app.domain.MessageDataTransformer;
import wierd_message_app.domain.User;

public class Main {
	
	public static void main(String[] args) {
		
		// skapa printer och g�r den redo
		
		UserMessagePrinter userMessagePrinter = new UserMessagePrinter();
		userMessagePrinter.addAllUsers(createUsersWithMessages());
		
		// printa!!
		
		userMessagePrinter.printAllMessages();
		
		userMessagePrinter.printAllInCategory(FUNNY);
		
		userMessagePrinter.printAllFromUserWithName("Fiona");
		
		
		// skapa en egen transformer (som �ndrar s�ttet som meddelanden skrivs ut p�)
		
		MessageDataTransformer toUpperCase = new MessageDataTransformer() {
			@Override
			public String transform(String data) {
				return data.toUpperCase();
			}
		};
		
		
		userMessagePrinter.setTransformer(toUpperCase);	
		userMessagePrinter.printAllMessages();
		
	}
	
	
	
	private static Set<User> createUsersWithMessages() {
		// Skapa anv�ndare
		
		User markus, ellen, fiona, kasper, sven, ingrid;
		markus = new User("Marcus");
		ellen = new User("Ellen");
		fiona = new User("Fiona");
		kasper = new User("Kasper");
		sven = new User("Sven");
		ingrid = new User("Ingrid");
		
		// Skapa meddelanden
		Message m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12;
		m1 = new Message("Det var norsken, tysken och Bellman, n�got kul h�nde.", FUNNY);
		m2 = new Message("Det var norsken, tysken och Bellman. Dom dog.", FUNNY, WEIRD, SERIOUS);
		m3 = new Message("Farv�l, grymma v�rld.", SERIOUS);
		m4 = new Message("Orange vind, i �gats vr�. Gr�t, medan du kan.", WEIRD, SERIOUS);
		
		m5 = new Message("Det var norsken, tysken och Karin.. ", FUNNY);
		m6 = new Message("Det var norsken, tysken och Karin. Dom dog.", FUNNY, WEIRD, SERIOUS);
		m7 = new Message("Hejsan svejsan, grymma v�rld.", SERIOUS, FUNNY, WEIRD);
		m8 = new Message("Rosa lukt, i n�sans vr�. Nys, medan du kan.", WEIRD, SERIOUS);
		
		m9 = new Message("Det var tysken, norsken och en motst�ndsr�relse.. ", SERIOUS);
		m10 = new Message("Det var norsken, tysken och Karin. Dom log.", FUNNY, WEIRD);
		m11 = new Message("Farv�l, gymma v�l.", FUNNY, WEIRD);
		m12 = new Message("Svart himmel. Svart sinne. Svart f�r g�r till slakt.", SERIOUS);
		
		// Koppla ihop
		
		markus.addAllMessages(Arrays.asList(m1, m2));
		ellen.addAllMessages(Arrays.asList(m3, m4));
		fiona.addAllMessages(Arrays.asList(m5, m6));
		kasper.addAllMessages(Arrays.asList(m7, m8));
		sven.addAllMessages(Arrays.asList(m9, m10));
		ingrid.addAllMessages(Arrays.asList(m11, m12));
		
		return new HashSet<User>(Arrays.asList(markus,ellen,fiona,kasper,sven,ingrid));
	}
	

}
