package wierd_message_app.main;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import wierd_message_app.domain.Message;
import wierd_message_app.domain.MessageCategory;
import wierd_message_app.domain.MessageDataTransformer;
import wierd_message_app.domain.User;

/**
 * 
 * Prints messages after passing text through a transformer (default transformer changes nothing)
 *
 */
public class UserMessagePrinter {
	private Set<User> users;
	private MessageDataTransformer transformer; // alla meddelanden
	
	public UserMessagePrinter() {
		users = new HashSet<User>();
		setTransformerToDefault();
	}
	
	public void printAllMessages() {
		for(User user : users) {
			for(Message message : user.getMessages()) {
				System.out.println(this.transformer.transform(message.getData()));
			}
		}
	}
	
	public void printAllInCategory(MessageCategory category) {
		System.out.println("\nBara meddelanden tillh�rande katagorin " + category);
		
		for(User user : users) {
			for(Message message : user.getMessages()) {
				if(message.getCategoryTags().contains(category)) {
					System.out.println(this.transformer.transform(message.getData()));
				}
			}
		}
	}
	
	public void printAllFromUserWithName(String name) {
		System.out.println("\nBara fr�n anv�ndare med namnet " + name);
		
		for(User user : users) {
			if(user.getName() == name) {
				for(Message message : user.getMessages()) {
					System.out.println(this.transformer.transform(message.getData()));
				}				
			}
		}
	}

	public boolean addAllUsers(Collection<? extends User> c) {
		return users.addAll(c);
	}
	
	public void setTransformer(MessageDataTransformer transformer) {
		this.transformer = transformer;
	}
	
	
	public void setTransformerToDefault() {
		// annonym klass
		transformer = new MessageDataTransformer() {
			@Override
			public String transform(String data) {
				return data; // g�r ingenting
			}
		};
	}
	
	
	
	
}
