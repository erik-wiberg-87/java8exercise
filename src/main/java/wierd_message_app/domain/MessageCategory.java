package wierd_message_app.domain;

/**
 * 
 * Categories a message can "belong to", such as FUNNY, SERIOUS, WEIRD
 *
 */
public enum MessageCategory {
	SERIOUS, FUNNY, WEIRD
}
