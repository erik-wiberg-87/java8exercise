package wierd_message_app.domain;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Representing a text-based message, with associated category tags and a time of creation
 *
 */
public class Message {
	private LocalDateTime timeCreated;
	private String data;
	private Set<MessageCategory> categoryTags;
	
	public Message(String data, MessageCategory... categoryTags) {
		this.timeCreated = LocalDateTime.now();
		this.data = data;
		this.categoryTags = new HashSet<MessageCategory>(Arrays.asList(categoryTags));
	}

	public LocalDateTime getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(LocalDateTime timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Set<MessageCategory> getCategoryTags() {
		return categoryTags;
	}
	
	@Override
	public String toString() {
		return String.format("\"%s\" \n\tcategories%s, \n\tcreated: %s", data, categoryTags.toString(), timeCreated.toString());
	}
	
}
