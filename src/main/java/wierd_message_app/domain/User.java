package wierd_message_app.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 
 * Representing a user with a name and a list of messages
 *
 */
public class User {
	private String name;
	private List<Message> messages;
	public User(String name) {
		this.name = name;
		messages = new ArrayList<Message>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Message> getMessages() {
		return messages;
	}
	
	public boolean addMessage(Message e) {
		return messages.add(e);
	}
	public boolean addAllMessages(Collection<? extends Message> c) {
		return messages.addAll(c);
	}
	
}
