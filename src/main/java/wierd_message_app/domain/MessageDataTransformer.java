package wierd_message_app.domain;

public interface MessageDataTransformer {
	String transform(String data);
}
